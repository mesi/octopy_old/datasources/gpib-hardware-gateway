from pymeasure.adapters import VISAAdapter
from pyvisa.errors import *
import logging
import time


class Command(object):
    def __init__(self, command, lnk=None, cid=0, no_reply=False):
        self.command = command.rstrip()
        self.no_reply = no_reply
        self.sent = False
        self.cid = cid
        self.reply = ''
        self.link = lnk
        self.wait_times = 2

    def __str__(self):
        return 'command: {} reply: {}\nlink:\n{}'.format(self.command, self.reply, self.link)


class GpibRequestResponseClient(object):

    def __init__(self, pollintervall=1, delimiter=b'\n'):
        self.command_que = []
        self.pollintervall = pollintervall
        self.has_idn = True
        self._visa_adapter = None
        self.address = None
        self.port = None
        self.timer = time.monotonic()
        self._on_overload = None
        self._on_message = None
        self._on_message_sent = None
        self._on_no_reply = None
        self._on_available = None
        self._on_error = None
        self.delimiter = delimiter
        self._disconnect = False
        self.last_loop_send = True
        self.run_loop = True

    def connect(self, address):
        # if isinstance(address, int):
        #     address = 'GPIB::' + str(address)
        # if len(address) == 0:
        #     raise ValueError('Invalid address.')
        self.address = address
        logging.info('Trying to connect to GPIB device with address {}'.format(self.address))
        return self.reconnect()

    def reconnect(self):
        try:
            kwargs = {'send_end': True, 'read_termination': self.delimiter.decode()}
            self._visa_adapter = VISAAdapter(self.address, **kwargs)
        except VisaIOError as err:
            print('ERROR: {}'.format(err))
            self.close()
        self._visa_adapter.flush_read_buffer()
        return True

    def disconnect(self):
        logging.info('Disconnecting GPIB device with address {}'.format(self.address))
        self._disconnect = True
        self.close()

    @property
    def on_message(self):
        return self._on_message

    @on_message.setter
    def on_message(self, func):
        self._on_message = func

    @property
    def on_message_sent(self):
        return self._on_message_sent

    @on_message_sent.setter
    def on_message_sent(self, func):
        self._on_message_sent = func

    @property
    def on_no_reply(self):
        return self._on_no_reply

    @on_no_reply.setter
    def on_no_reply(self, func):
        self._on_no_reply = func

    @property
    def on_available(self):
        return self._on_available

    @on_available.setter
    def on_available(self, func):
        self._on_available = func

    @property
    def on_overload(self):
        return self._on_overload

    @on_overload.setter
    def on_overload(self, func):
        self._on_overload = func

    def send(self):
        if self.command_que:
            logging.debug('The length of the command que is: {}'.format(len(self.command_que)))
            if time.monotonic() - self.timer > self.pollintervall:
                if len(self.command_que) > 1 and self.on_overload:
                    if self.on_overload:
                        self.on_overload()
                    logging.error('polling too fast, ' + str(len(self.command_que)) + ' commands waiting.')
                command = self.command_que[0]
                if command.sent and command.reply == "":
                    if self._on_no_reply:
                        self.on_no_reply(command)
                    logging.warning('No reply from: <{}>'.format(command.command))
                    if command.wait_times <= 0:
                        self.command_que.pop(0)
                        logging.warning('No reply from: <{}> giving up'.format(command.command))
                        return
                    else:
                        logging.warning('No reply from: <{}> retrying'.format(command.command))
                        command.wait_times -= 1
                cmd_to_send = command.command
                # if not cmd_to_send.endswith(self.delimiter):
                #     cmd_to_send += self.delimiter
                logging.info('Sending command: {}'.format(command.command))
                command.reply = self._visa_adapter.ask(cmd_to_send.decode())
                command.sent = True
                if command.reply and not command.no_reply:
                    logging.info('Received: {}'.format(command.reply))
                    if self.on_message:
                        self.on_message(self, command)
                else:
                    logging.warning('No reply from command: {}: {}, should not reply?: {}'.format(command.command, command.reply, command.no_reply))
                if self.on_available:
                    self.on_available()
                self.command_que.pop(0)
                if self.on_message_sent:
                    self.on_message_sent(command)

    def loop(self):
        if not self.run_loop:
            return True
        if self._visa_adapter:
            try:
                self.send()
            except BrokenPipeError as err:
                logging.error('GPIB_RequestResponseClient.loop BrokenPipeError: {}'.format(err))
                self.close()
            except OSError as err:
                logging.error('GPIB_RequestResponseClient.loop OSError: {}'.format(err))
                self.close()
        elif self._disconnect:
            return False
        else:
            logging.info('Trying to reconnect in 3s')
            time.sleep(3)
            self.reconnect()
        return True

    def close(self):
        logging.info('Closing visa adapter on address {}'.format(self.address))
        if self._visa_adapter:
            self._visa_adapter = None
            return
        else:
            logging.info('GPIB_request_response.close(): No adapter to close')
            return

    def append_to_que(self, command, top_of_que=False):
        """ Add a command to the que, a set reference is a command you would like to put on top of the que"""
        if isinstance(command, Command):
            logging.info('Appending to que: {}'.format(command.command))
            if top_of_que:
                self.command_que.insert(0, command)
            else:
                self.command_que.append(command)
        else:
            raise ValueError('You can only append a {}.Command'.format(command))

    def test_command(self, test_command):
        logging.info('Testing command: {}'.format(test_command))
        idn = None
        reply = None
        self.run_loop = False
        if not isinstance(test_command, bytes):
            test_command = test_command.encode()
        if self.has_idn:
            try:
                idn = self._visa_adapter.ask('*IDN?')
            except VisaIOError as err:
                logging.warning('IDN command not available')
                self.has_idn = False
        # if not (test_command.endswith(b'\r') or test_command.endswith(b'\n')):
        #     test_command += self.delimiter
        #     print('________________added delimiter_________________')
        try:
            reply = self._visa_adapter.ask(test_command.rstrip().decode())
        except VisaIOError as err:
            logging.error(err)
            return None
        self.run_loop = True
        if reply == idn and self.has_idn:
            return None
        return reply


if __name__ == '__main__':
    import sys
    logging.basicConfig(level=10, stream=sys.stdout)

    def on_tcprrc_msg(client, command):
        print('Client: {}, message: {}'.format(client.host, command.reply.decode()))

    gpibrrc = GpibRequestResponseClient(delimiter=b'')
    gpibrrc.connect("GPIB::12")
    gpibrrc.on_message = on_tcprrc_msg

    while True:
        print('loop')
        time.sleep(1)
        command = input('Send command: ').encode()
        print(command)
        print(gpibrrc.test_command(command))
        # gpibrrc.append_to_que(command)
        # gpibrrc.loop()
